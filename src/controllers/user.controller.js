'use strict';
var mongoose = require('mongoose');
var moment = require('moment');
var request = require('request').defaults({encoding: null});
const userService = require('../services/user.service');
const friendshipService = require('../services/friendship.service');
const lessonService = require('../services/lesson.service');

let userController = {
    getById: async function(req, res){
         if ( !mongoose.Types.ObjectId.isValid(req.params.id) )
            res.status(400).send({message: 'id badly formatted'});
        else {
            let u = await userService.getById(req.params.id)
            if (u) res.send(u);
            else res.status(404).send({message: 'not found'});
        }
    },
    getByIdLessons: async function(req, res) {
        if ( !mongoose.Types.ObjectId.isValid(req.params.id) )
            res.status(400).send({message: 'id badly formatted'});
        else {
            let u = await userService.getById(req.params.id);
            if (!u) { res.status(404).send({message: 'user not found'}); return; }
            res.send(await userService.getLessons(req.params.id));

        }
    },
    addLesson: async function(req, res) {
        if ( !mongoose.Types.ObjectId.isValid(req.params.id) && !mongoose.Types.ObjectId.isValid(req.body.id) )
            res.status(400).send({message: 'body badly formatted'});
        else {
            let u = await userService.getById(req.params.id);
            let l = await lessonService.getById(req.body.id);
            if (!u || !l)
              res.status(422).send({message: 'unprocessable'});
            else {
              let d = await userService.addLesson(u,l,req.body);
              res.status(201).send({message: 'created', payload: d._id.toString()});
            }
        }
    },
    addFriend: async function(req, res){
        if ( !mongoose.Types.ObjectId.isValid(req.params.id) && !mongoose.Types.ObjectId.isValid(req.body.to) )
            res.status(400).send({message: 'body badly formatted'});
        else {
            let to = await userService.getById(req.body.to);
            let from = await userService.getById(req.params.id);
            if (!from || !to)
              res.status(422).send({message: 'unprocessable'});
            else  {
              let f = await friendshipService.create(from, to, {} );
              if (f) res.status(201).send({message: 'created',payload: f});
              else return res.status(500).send({message: 'unhandled error'});
            }
        }
    },
    getByIdFriends: async function(req, res){
        if ( !mongoose.Types.ObjectId.isValid(req.params.id) )
            res.status(400).send({message: 'id badly formatted'});
        else {
            let u = await userService.getById(req.params.id);
            if (!u) { res.status(404).send({message: 'user not found'}); return; }
            let r = await friendshipService.getByUserId(u);
            res.status(200).send(r);
        }
    },
    create: async function(req, res){
        if ( !req.body || !req.body.name || req.body.name == '' ) {
            res.status(400).send({message: 'body badly formatted'});
        }
        else {
            let d = await userService.create(req.body);
            res.status(201).send({message: 'created', payload: d._id.toString()});
        }
    },
    list: async function(req, res){
        if ( false )
            res.status(400).send({message: 'params badly formatted'});
        else {
            let currentPage = req.query.page? parseInt(req.query.page): 0;
            let currentPageSize = req.query.pageSize? parseInt(req.query.pageSize):10;
            let currentSearchText = req.query.search || '';
            let d = await userService.list({ page: currentPage, pageSize: currentPageSize, search: currentSearchText} );
            res.status(200).send(d);
        }
    },
    update: async function(req, res){
        if ( !mongoose.Types.ObjectId.isValid(req.params.id) )
            res.status(400).send({message: 'id badly formatted'});
        else            {
            let d = await userService.update(req.params.id, req.body);
            res.status(200).send({message: 'updated', payload: d._id.toString()});
            }
    },
    patch: async function(req, res){
        if ( !mongoose.Types.ObjectId.isValid(req.params.id) )
            res.status(400).send({message: 'id badly formatted'});
        else  {
            let d = await userService.patch(req.params.id, req.body)
            if (d)
                res.status(200).send({message: 'patched', payload: d._id.toString()});
            else  {
                res.status(204).send();

            }
        }
    }

}

module.exports = userController;
