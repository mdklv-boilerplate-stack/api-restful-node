'use strict';
var mongoose = require('mongoose');
var moment = require('moment');
var request = require('request').defaults({encoding: null});
const friendshipService = require('../services/friendship.service');
const userService = require('../services/user.service');

let FriendshipController = {
    getById: async function(req, res){
        if ( !mongoose.Types.ObjectId.isValid(req.params.id) )
            res.status(400).send({message: 'id badly formatted'});
        else
            res.send(await friendshipService.getById(req.params.id));
    },
    list: async function(req, res){
        if (
            (req.query && req.query.from && !mongoose.Types.ObjectId.isValid(req.query.from)) ||
            (req.query && req.query.to && !mongoose.Types.ObjectId.isValid(req.query.to))
           )
            res.status(400).send({message: 'params badly formatted'});
        else {
          let currentPage = req.query.page? parseInt(req.query.page): 0;
          let currentPageSize = req.query.pageSize? parseInt(req.query.pageSize):10;
          let currentFrom = req.query.from?req.query.from:'';
          let currentTo = req.query.to?req.query.to:'';
          let currentSearchText = req.query.search || '';

          let d = await friendshipService.list({ page: currentPage, pageSize: currentPageSize, from: currentFrom, to: currentTo, search: currentSearchText} );
          res.status(200).send(d);
        }
    },
    create: async function(req, res){
        if ( !mongoose.Types.ObjectId.isValid(req.body.from) || !mongoose.Types.ObjectId.isValid(req.body.to) )
            res.status(400).send({message: 'body badly formatted'});
        else {
            let ufrom = userService.getById(req.body.from);
            let uto = userService.getById(req.body.to);
            if (!ufrom || !uto)
              res.status(422).send({message: 'unprocessable entity'});
            else {
              let d = await friendshipService.create(uFrom, uTo, req.body );
              if (d) res.status(201).send({message: 'created',payload: d});
              else return res.status(500).send({message: 'unhandled error'});
            }
        }
    },
    update: async function(req, res){
        if ( !mongoose.Types.ObjectId.isValid(req.params.id) )
            res.status(400).send({message: 'id badly formatted'});
        else
            {
            let d = await friendshipService.update(req.params.id, req.body);
            res.status(200).send({message: 'updated', payload: d._id.toString()});
            }
    },
    patch: async function(req, res){
        if ( !mongoose.Types.ObjectId.isValid(req.params.id) )
            res.status(400).send({message: 'id badly formatted'});
        else  {
            let d = await friendshipService.patch(req.params.id, req.body)
            if (d)
                res.status(200).send({message: 'patched', payload: d._id.toString()});
            else  {
                res.status(204).send();

            }
        }
    },

}

module.exports = FriendshipController;
