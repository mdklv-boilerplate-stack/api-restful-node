'use strict';
var mongoose = require('mongoose');
var moment = require('moment');
var request = require('request').defaults({encoding: null});
const lessonService = require('../services/lesson.service');

let LessonController = {
    getById: async function(req, res){
        if ( !mongoose.Types.ObjectId.isValid(req.params.id) )
            res.status(400).send({message: 'id badly formatted'});
        else {
          let l = await lessonService.getById(req.params.id)
          if (l) res.send(l);
          else res.status(404).send({message: 'not found'});
        }
    },
    list: async function(req, res){
        if ( false /*!mongoose.Types.ObjectId.issValid(req.params.id)*/ )
            res.status(400).send({message: 'params badly formatted'});
        else {
            let currentPage = req.query.page? parseInt(req.query.page): 0;
            let currentPageSize = req.query.pageSize? parseInt(req.query.pageSize):10;
            let currentSearchText = req.query.search || '';
            let d = await lessonService.list({ page: currentPage, pageSize: currentPageSize, search: currentSearchText} );
            res.status(200).send(d);
        }
    },
    create: async function(req, res){
        if ( !req.body || !req.body.name || req.body.name == '' )
            res.status(400).send({message: 'body badly formatted'});
        else {
            let d = await lessonService.create(req.body);
            res.status(201).send({message: 'created', payload: d._id.toString()});
        }
    },
    update: async function(req, res){
        if ( !mongoose.Types.ObjectId.isValid(req.params.id) )
            res.status(400).send({message: 'id badly formatted'});
        else
            {
            let d = await lessonService.update(req.params.id, req.body);
            res.status(200).send({message: 'updated', payload: d._id.toString()});
            }
    },
    patch: async function(req, res){
        if ( !mongoose.Types.ObjectId.isValid(req.params.id) )
            res.status(400).send({message: 'id badly formatted'});
        else  {
            let d = await lessonService.patch(req.params.id, req.body)
            if (d)
                res.status(200).send({message: 'patched', payload: d._id.toString()});
            else  {
                res.status(204).send();

            }
        }
    },

}

module.exports = LessonController;
