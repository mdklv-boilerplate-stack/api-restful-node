const Sentry = require("@sentry/node");
const Tracing = require("@sentry/tracing");
const init = async (app) => {
    if (process.env && process.env.SENTRY_DNS) {
      Sentry.init({
          dsn: process.env.SENTRY_DSN,
          integrations: [
              new Sentry.Integrations.Http({ tracing: true }),
              new Tracing.Integrations.Express({ app }),
          ],
          tracesSampleRate: 1.0,
      });

      app.use(Sentry.Handlers.requestHandler());
      app.use(Sentry.Handlers.tracingHandler());
      app.use(
          Sentry.Handlers.errorHandler({
              shouldHandleError(error) {
                  // Capture all 404 and 500 errors
                  if (error.status === 404 || error.status === 500) {
                      return true;
                  }
                  return false;
              },
          })
      );
      app.use(function onError(err, req, res, next) {
          console.error("error tracked");
          console.error(err);
          res.statusCode = 500;
          res.end(res.sentry + "\n");
      });
    }
}
const captureException = async(e) => {
  if (process.env && process.env.SENTRY_DNS) {
    Sentry.captureException(e);
  } else {
    console.error(e);
  }
}
module.exports = {
  init,
  captureException
}
