const CaptureException = require('../lib/sentry.js');
const Friendship = require('../models/friendship');
var mongoose = require('mongoose')
    , Schema = mongoose.Schema;
const ObjectId = mongoose.Types.ObjectId;

exports.getById = async ( id ) => {
    try {
        return await _getById(  id );
    }catch (e){
        CaptureException.captureException(e);
        throw new Error(e);
    }
}
exports.getByUserId = async ( id ) => {
    try {
        return await _getByUserId(  id );
    }catch (e){
        CaptureException.captureException(e);
        throw new Error(e);
    }
}

exports.list = async (params ) => {
  try {
      return await _list( params );
  }catch (e){
      CaptureException.captureException(e);
      throw new Error(e);
  }
}
exports.create = async ( userFrom, userTo, payload ) => {
    try {
        return await _create( userFrom, userTo, payload );
    }catch (e) {
        CaptureException.captureException(e);
        throw new Error(e);

    }
}
exports.update = async ( id, payload ) => {
    try{
        return await updateFromJSON( id, payload );
    }catch (e) {
        CaptureException.captureException(e);
        throw new Error(e);
    }
}
exports.patch = async( id, payload ) => {
    try {
        return await updateFromElements( id, payload );
    }catch (e) {
        CaptureException.captureException(e);
        throw new Error(e);
    }
}

const _create = async( userFrom, userTo, payload ) => {
    let r1,r2;
    let l1 = await _list( {from: userFrom.id, to: userTo.id} );

    if (l1 && l1.length>0) r1 = l1[0];
    else {
        let fsRel1 = new Friendship();
        fsRel1.from = userFrom.id;
        fsRel1.to = userTo.id;
        fsRel1.requested = new Date().toISOString();
        fsRel1.accepted = new Date().toISOString();
        r1 = await fsRel1.save();
    }
    let l2 = await _list( {from: userTo.id, to: userFrom.id} );
    if (l2 && l2.length>0) r2 = l2[0]
    else {
      let fsRel2 = new Friendship();
      fsRel2.from = userTo.id;
      fsRel2.to = userFrom.id;
      fsRel2.requested = new Date().toISOString();
      fsRel2.accepted = new Date().toISOString();
      r2 = await fsRel2.save();
    }
    if (r1 && r2) return `${r1._id}, ${r2._id}`
    else return null;
}
const _getById = async( id ) => {
    return await Friendship.findOne({'_id': id}).select("-_id -__v");
}
const updateFromJSON = async (id, json) => {
    return await Friendship.findOneAndUpdate({'_id': id} ,json ).save();
}
const updateFromElements = async( id, elements ) => {
    if (elements) {
        let d = await Friendship.findOne({'_id': id}).populate();
        for(let i=0;i<d.detail.length;i++){
            let idx = elements.findIndex( s => s.key == d.detail[i].key );
            if (idx != -1){
                d.detail[i].value = elements[idx].value;
            }
        }
        return await d.save();
    } else {
       CaptureException.captureException(e);
       throw new Error("not found");
    }
}


const _list = async( params ) => {
    let perPage = params.pageSize?params.pageSize:10;
    let page = Math.max(0, params.page?params.page:0);
    let skipE = (params.page?params.page:0) * perPage;

    let aExprFilter = [];
    if (params.from && params.from != '' ) aExprFilter.push( { "from":  params.from } );
    if (params.to && params.to != '' ) aExprFilter.push( { "to": params.to } );
    let exprFilter = aExprFilter.length > 0 ? { $match: { $and: aExprFilter } }: undefined;

    let aExprText = [];
    if (params.search != '' ) {
        aExprText.push( { "from.name": new RegExp( params.search, 'i' ) } );
        aExprText.push( { "to.name": new RegExp( params.search, 'i' ) } );
    }
    let exprText = aExprText.length > 0 ? { $match: { $or: aExprText } }: undefined;

    let pipeline = [
      {
          $project: {
              _id: 0,
              from: 1,
              to: 1,
              requested: 1,
              accepted: 1,
              type: 1
          }
      },
      exprFilter,
      {
        $lookup: { from: 'user', localField: 'from', foreignField: '_id', as: 'tmpfrom' }
      },
      {
        $unwind: '$tmpfrom'
      },
      {
        $lookup: { from: 'user', localField: 'to', foreignField: '_id', as: 'tmpto' }
      },
      {
        $unwind: '$tmpto'
      },
      {
        $addFields: {
                  "from.id": "$tmpfrom._id",
                  "from.name": "$tmpfrom.name",
                  "to.id": "$tmpto._id",
                  "to.name": "$tmpto.name",
        }
      },
      exprText,
      {
        $project: {
            tmpto: 0,
            tmpfrom: 0
        }
      },
      { $sort: { "requested": 1 } },
      { $limit: skipE + perPage },
      { $skip:  skipE }
    ].filter(Boolean);

    let friendships = await Friendship.aggregate( pipeline );
    return friendships;
}

const _getByUserId = async( user ) =>{
    let l = await _list( { pageSize: 100, page: 0, from: user.id, to: undefined, search: undefined });
    return Array.from(l).map(x=> {return x.to });
}
