const CaptureException = require('@sentry/node');
const User = require('../models/user');
const Course = require('../models/course');
var mongoose = require('mongoose')
    , Schema = mongoose.Schema;
const ObjectId = mongoose.Types.ObjectId;

exports.getById = async ( id ) => {
    try {
        return await _getById(  id );
    }catch (e){
        CaptureException.captureException(e);
        throw new Error(e);
    }
}
exports.list = async (params ) => {
  try {
      return await _list( params );
  }catch (e){
      CaptureException.captureException(e);
      throw new Error(e);
  }
}
exports.create = async ( payload ) => {
    try {
        return await createFromJSON( payload );
    }catch (e) {
        CaptureException.captureException(e);
        throw new Error(e);

    }
}
exports.update = async ( id, payload ) => {
    try {
        return await updateFromJSON( id, payload );
    }catch (e) {
        CaptureException.captureException(e);
        throw new Error(e);
    }
}
exports.patch = async( id, payload ) => {
    try {
        return await updateFromElements( id, payload );
    }catch (e) {
        CaptureException.captureException(e);
        throw new Error(e);
    }
}

exports.addLesson = async( id, payload ) => {
    try {
        return await _addLesson( id, payload );
    }catch (e) {
        CaptureException.captureException(e);
        throw new Error(e);
    }
}

exports.getLessons = async( id ) => {
    try {
        return await _getLessons( id );
    }catch (e) {
        CaptureException.captureException(e);
        throw new Error(e);
    }
}

const createFromJSON = async( json ) => {
    return await new User( json ).save();
}
const _getById = async( id ) => {
    let a = await _list( { id: id })
    if (a && a.length > 0) return a[0]
    else return null;
}
const updateFromJSON = async (id, json) => {
    return await User.findOneAndUpdate({'_id': id} ,json ).save();
}
const updateFromElements = async( id, elements ) => {
    if (elements) {
        let d = await User.findOne({'_id': id}).populate();
        for(let i=0;i<d.detail.length;i++){
            let idx = elements.findIndex( s => s.key == d.detail[i].key );
            if (idx != -1){
                d.detail[i].value = elements[idx].value;
            }
        }
        return await d.save();
    } else {
       CaptureException.captureException(e);
       console.log(e);
       throw new Error("not found");
    }
}

const _list = async( params ) => {
    let perPage = params.pageSize? params.pageSize: 10;
    let page = Math.max(0, params.page?params.page:0);
    let skipE = page * perPage;
    let aExprFilter = [];
    if (params.id && params.id != '' ) aExprFilter.push( { "_id": ObjectId(params.id) } );
    let exprFilter = aExprFilter.length > 0 ? { $match: { $and: aExprFilter } }: undefined;

    let aExprText = [];
    if (params.search != '' ) aExprText.push( { "name": new RegExp( params.search, 'i' ) } );
    let exprText = aExprText.length > 0 ? { $match: { $and: aExprText } }: undefined;
    let pipeline = [
      exprFilter,
      {
          $project: {
              _id: 0,
              id: '$_id',
              name: 1
          }
      },
      exprText,
      { $sort: { "name": -1 } },
      { $limit: skipE + perPage },
      { $skip:  skipE }
    ].filter(Boolean);
    let users = await User.aggregate( pipeline );
    return users;
}

const _addLesson = async( user , lesson, additionalPayload ) => {
    let c = new Course();
    c.user = ObjectId(user.id);
    c.lesson = ObjectId(lesson.id);
    c.createdAt = new Date().toISOString();
    return await c.save();
}


const _getLessons = async( id  ) => {
    let aExprFilter = [];
    if (id && id != '' ) aExprFilter.push( { "user": ObjectId(id)  } );
    let exprFilter = aExprFilter.length > 0 ? { $match: { $and: aExprFilter } }: undefined;
    let skipE = 0;
    let perPage = 1000;

    let pipeline = [
      {
          $project: {
              _id: 0,
              id: '$_id',
              user:1,
              lesson:1,
              createdAt:1
          }
      },
      exprFilter,
      {
          $lookup: { from: 'lesson', localField: 'lesson', foreignField: '_id', as: 'tmplesson' }
      },
      {
        $unwind: "$tmplesson"
      },
      {
        $addFields: {
          "id": "$tmplesson._id",
          "name": "$tmplesson.name"
        }
      },
      { $sort: { "createdAt": -1 } },
      {
        $project: {
            tmplesson: 0,
            lesson:0,
            user: 0,
            createdAt: 0
        }
      },
      { $limit: skipE + perPage },
      { $skip:  skipE }
    ].filter(Boolean);

    let courses = await Course.aggregate( pipeline );
    return courses;

}
