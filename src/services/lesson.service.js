const CaptureException = require('@sentry/node');
const Lesson = require('../models/lesson');
var mongoose = require('mongoose')
    , Schema = mongoose.Schema;
const ObjectId = mongoose.Types.ObjectId;

exports.getById = async ( id ) => {
    try {
        return await _getById(  id );
    }catch (e){
        CaptureException.captureException(e);
        throw new Error(e);
    }
}

exports.list = async (params ) => {
  try {
      return await _list( params );
  }catch (e){
      CaptureException.captureException(e);
      throw new Error(e);
  }
}
exports.create = async ( payload ) => {
    try {
        return await createFromJSON( payload );
    }catch (e) {
        CaptureException.captureException(e);
        throw new Error(e);

    }
}
exports.update = async ( id, payload ) => {
    try {
        return await updateFromJSON( id, payload );
    }catch (e) {
        CaptureException.captureException(e);
        throw new Error(e);
    }
}
exports.patch = async( id, payload ) => {
    try {
        return await updateFromElements( id, payload );
    }catch (e) {
        CaptureException.captureException(e);
        throw new Error(e);
    }
}

const createFromJSON = async( json ) => {
    return await new Lesson( json ).save();
}
const _getById = async( id ) => {
    let a = await _list( { id: id })
    if (a && a.length > 0) return a[0]
    else return null;
}
const updateFromJSON = async (id, json) => {
    return await Lesson.findOneAndUpdate({'_id': id} ,json ).save();
}
const updateFromElements = async( id, elements ) => {
    if (elements) {
        let d = await Lesson.findOne({'_id': id}).populate();
        for(let i=0;i<d.detail.length;i++){
            let idx = elements.findIndex( s => s.key == d.detail[i].key );
            if (idx != -1){
                d.detail[i].value = elements[idx].value;
            }
        }
        return await d.save();
    } else {
       CaptureException.captureException(e);
       throw new Error("not found");
    }
}

const _list = async( params ) => {
    let perPage = params.pageSize? params.pageSize: 10;
    let page = Math.max(0, params.page?params.page:0);
    let skipE = page * perPage;

    let aExprFilter = [];
    if (params.id && params.id != '' ) aExprFilter.push( { "_id": ObjectId(params.id) } );
    let exprFilter = aExprFilter.length > 0 ? { $match: { $and: aExprFilter } }: undefined;

    let aExprText = [];
    if (params.search && params.search != '' ) aExprText.push( { "name": new RegExp( params.search, 'i' ) } );
    let exprText = aExprText.length > 0 ? { $match: { $and: aExprText } }: undefined;

    let pipeline = [
      exprFilter,
      {
          $project: {
              _id: 0,
              id: '$_id',
              name: 1
          }
      },
      exprText,
      { $sort: { "name": -1 } },
      { $limit: skipE + perPage },
      { $skip:  skipE }
    ].filter(Boolean);

    let lessons = await Lesson.aggregate( pipeline );
    return lessons;
}
