var mongoose = require('mongoose')
    , Schema = mongoose.Schema;

const lessonSchema = new mongoose.Schema(
    {
      name: {
        type: String
      }
    }
,{toJSON: { virtuals: true }, autoCreate: true , versionKey: false});
const Lesson = mongoose.model("Lesson", lessonSchema, "lesson");
module.exports = Lesson;
