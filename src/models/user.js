var mongoose = require('mongoose')
    , Schema = mongoose.Schema;

const userSchema = new mongoose.Schema(
  {
    name: {
      type: String
    }
  }
,{toJSON: { virtuals: true }, autoCreate: true , versionKey: false});
userSchema.virtual('id').get(function(){
    return this._id;
});

const User = mongoose.model("User", userSchema, "user");

module.exports = User;
