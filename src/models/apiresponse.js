var mongoose = require('mongoose')
    , Schema = mongoose.Schema;

const apiresponseSchema = new mongoose.Schema(
    {
      "code": {
        "type": "Number"
      },
      "payload": {
        "type": "String"
      },
      "message": {
        "type": "String"
      }
    }
,{toJSON: { virtuals: true }, autoCreate: true , versionKey: false});
const ApiResponse = mongoose.model("ApiResponse", apiresponseSchema, "apiresponse");
module.exports = ApiResponse;
