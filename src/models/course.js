var mongoose = require('mongoose')
    , Schema = mongoose.Schema;

const courseSchema = new mongoose.Schema(
  {
      user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
      },
      lesson: {
        type: Schema.Types.ObjectId,
        ref: 'Lesson',
      },
      createdAt: {
        type: String
      }
    }
,{toJSON: { virtuals: true }, autoCreate: true , versionKey: false});
const Course = mongoose.model("Course", courseSchema, "course");
module.exports = Course;
