var mongoose = require('mongoose')
    , Schema = mongoose.Schema;

const friendshipSchema = new mongoose.Schema(
  {
      from: {
        type: Schema.Types.ObjectId,
        ref: 'User',
      },
      to: {
        type: Schema.Types.ObjectId,
        ref: 'User',
      },
      requested: {
        type: String
      },
      accepted: {
        type: String
      },
      type: {
        type: String
      }
    }
,{toJSON: { virtuals: true }, autoCreate: true , versionKey: false});
const Friendship = mongoose.model("Friendship", friendshipSchema, "friendship");
module.exports = Friendship;
