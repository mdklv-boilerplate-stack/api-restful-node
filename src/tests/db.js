const mongoose = require('mongoose');
import { MongoMemoryServer } from 'mongodb-memory-server';

// This will create an new instance of "MongoMemoryServer" and automatically start it
const mongod = new MongoMemoryServer();


module.exports.connect = async () => {
    await mongod.start();
    const uri =  mongod.getUri();
    const mongooseOption = {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }
    await mongoose.connect(uri, mongooseOption);
    mongoose.connection.on('error', (e) => {
        console.error(e);
    });

    mongoose.connection.once('open', () => {
        console.log(`MongoDB successfully connected to ${mongoUri}`);
    });
}

module.exports.close = async () => {
    await mongoose.connection.dropDatabase();
    await mongoose.connection.close();
    await mongod.stop();
}

module.exports.clear = async () => {
    const collections = mongoose.connection.collections;
    for(const key in collections){
        const collection = collections[key];
        await collection.deleteMany();
    }
}
