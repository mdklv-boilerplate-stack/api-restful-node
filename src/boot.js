const compression = require("compression");
const helmet = require("helmet");
const morgan = require("morgan");
const initMongo = require("./config/db");
const cookieParser = require('cookie-parser');
const initSentry = require('./lib/sentry')
const admin = require("firebase-admin");

const boot = async (express, app) => {
    await initUtils(express, app);
    await initMongo();
    await initSentry.init(app);
    await initializeFirebase();
}

const initUtils = async (express, app) => {
    app.use(express.json());
    app.use(express.urlencoded({ extended: false }));
    app.use(cookieParser("el_secreto_de_mop"));
    app.use(morgan(function (tokens, req, res) {
        return [
            tokens.method(req, res),
            tokens.url(req, res),
            tokens.status(req, res),
            tokens.res(req, res, 'content-length'), '-',
            tokens['response-time'](req, res), 'ms'
        ].join(' ')
    }));
    app.use(compression());
    app.use(helmet())
}
const initializeFirebase = async () => {
    if (process.env && process.env.FIREBASE_CONFIG_JSON && process.env.FIREBASE_CONFIG_JSON != "") {
      const serviceAccount =  JSON.parse( `${process.env.FIREBASE_CONFIG_JSON}` );
      admin.initializeApp({
          credential: admin.credential.cert(serviceAccount),
          databaseURL: process.env.FIREBASE_DATABASE_URL
      });
    }
}

module.exports = boot;
