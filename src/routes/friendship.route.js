'use strict';

let router = require('express').Router();
let cors = require('cors');

let corsOptions = {
    origin: '*',
    preflightContinue: true,
    allowedHeaders: ['Content-Type','Accept','Origin','X-Mop-Agent','X-Mop-Authorization'],
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}
let FriendshipController = require('../controllers/friendship.controller');
//const {authorize} = require("../../auth.js");
//router.post('/', cors(corsOptions), authorize, DocumentController.create );

router.post('/', cors(corsOptions),  FriendshipController.create );
router.put('/:id', cors(corsOptions),  FriendshipController.update);
router.patch('/:id', cors(corsOptions),  FriendshipController.patch);
router.get('/:id', cors(corsOptions),  FriendshipController.getById);
router.get('/', cors(corsOptions),  FriendshipController.list);

module.exports = router;
