'use strict';

let router = require('express').Router();
let cors = require('cors');

let corsOptions = {
    origin: '*',
    preflightContinue: true,
    allowedHeaders: ['Content-Type','Accept','Origin','X-Mop-Agent','X-Mop-Authorization'],
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}
let UserController = require('../controllers/user.controller');
//const {authorize} = require("../../auth.js");

router.get('/', cors(corsOptions),  UserController.list);
router.post('/', cors(corsOptions),  UserController.create );
router.get('/:id', cors(corsOptions),  UserController.getById);

router.get('/:id/lesson', cors(corsOptions),  UserController.getByIdLessons);
router.post('/:id/lesson', cors(corsOptions),  UserController.addLesson);
router.get('/:id/friend', cors(corsOptions),  UserController.getByIdFriends);
router.post('/:id/friend', cors(corsOptions),  UserController.addFriend);

router.put('/:id', cors(corsOptions),  UserController.update);
router.patch('/:id', cors(corsOptions),  UserController.patch);



module.exports = router;
