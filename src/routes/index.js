let cors = require('cors');

const routes = (app) => {
    app.use('/user' , require('./user.route') );
    app.use('/friendship' , require('./friendship.route') );
    app.use('/lesson' , require('./lesson.route') );

    app.get('/', cors(), async (_req, res, _next) => {
        const healthcheck = {
            uptime: process.uptime(),
            message: 'OK',
            environment: process.env.NODE_ENV,
            timestamp: Date.now()
        };
        try {
            res.send(healthcheck);
        } catch (e) {
            healthcheck.message = e;
            res.status(503).send();
        }
    });
}
module.exports = routes;
