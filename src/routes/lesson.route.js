'use strict';

let router = require('express').Router();
let cors = require('cors');

let corsOptions = {
    origin: '*',
    preflightContinue: true,
    allowedHeaders: ['Content-Type','Accept','Origin','X-Mop-Agent','X-Mop-Authorization'],
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}
let LessonController = require('../controllers/lesson.controller');
//const {authorize} = require("../../auth.js");
//router.post('/', cors(corsOptions), authorize, DocumentController.create );

router.post('/', cors(corsOptions),  LessonController.create );
router.put('/:id', cors(corsOptions),  LessonController.update);
router.patch('/:id', cors(corsOptions),  LessonController.patch);
router.get('/:id', cors(corsOptions),  LessonController.getById);
router.get('/', cors(corsOptions),  LessonController.list);

module.exports = router;
