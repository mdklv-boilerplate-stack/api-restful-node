const {RateLimiterMemory, RateLimiterRes} = require("rate-limiter-flexible");
const express = require('express')
const compression = require('compression')
const boot = require("./boot");
const app = express()
const port = process.env.PORT;
const host = "0.0.0.0";
const routes = require('./routes/index')
const swaggerUi = require('swagger-ui-express')


const cors = require('cors')
const swaggerDocument = require('../openapi.json');
// Boot initial config
boot(express, app);

// Load all routes
app.set("view engine", "ejs");
app.use(cors());
app.use(compression());
const opts = {
    points: 10, // 10 request
    duration: 1, // Per second
    blockDuration: 1 * 60, // block for 1 minute next request
};
const rateLimiter = new RateLimiterMemory(opts);

app.use(function (req, res, next) {
//    rateLimiter.consume(req.ip, 1) // consume 2 points
//        .then((rateLimiterRes) => {
            next();
//        })
//        .catch((rateLimiterRes) => {
//            res.sendStatus(429)
//        });
});

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", '*');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use( express.static( "public" ) );
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, {
    explorer: true
}))
routes(app);

app.listen(port,host);

module.exports = app;
