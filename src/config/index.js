'use strict';

const dotenv = require('dotenv');
dotenv.config();
var config = module.exports;
config.db = {
    endpoint: process.env.MONGO_HOST,
    database: process.env.MONGO_DATABASE,
};

config.origin = {
    host: process.env.ORIGIN,
}


