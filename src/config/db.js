const mongoose = require("mongoose");
const fs = require('fs');
let config = require('./index');
const Sentry = require('@sentry/node')
const path = require('path');
const options = require('./config.' + process.env.NODE_ENV);
const MongoServer = async () => {
    try {
        console.log("Data to connect document db")
        console.log(process.env.MONGO_HOST);
        /*if(process.env.NODE_ENV === 'prod') {
            options.mongo.dbConfig.sslCA = path.join(__dirname, './docdb.pem').toString()
        }*/
        console.log(options)
        await mongoose.connect(config.db.endpoint, options.mongo.dbConfig,
         function (res){
            if(res !== null){
                Sentry.addBreadcrumb('Fail on connect with mongo');
                Sentry.captureException(res);
            }
        });
        const db = mongoose.connection;
        db.on("error", console.error.bind(console, "connection error: "));
        db.once("open", function () {
            console.log("Connected successfully");
        });
        mongoose.connection.on('reconnected', () => console.log('dbevent: reconnected'))


    } catch (e) {
        console.error(e);
        throw e;
    }
};


module.exports = MongoServer;
