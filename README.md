


# Test API

![version](https://img.shields.io/badge/version-1.0.0-blue.svg)
![build](https://img.shields.io/badge/build-passing-green.svg)
![tests](https://img.shields.io/badge/tests-in%20review-red.svg)

nodejs api

## Getting Started

### Previous requirements
* [Docker CE version 18.09.04 or up](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
* [Docker Compose version 1.24.0 or up](https://docs.docker.com/compose/install/)
* [Git client](https://git-scm.com/)

### Clone or manage your code in Fork of the original reposiory
You can use simply doing a clone of the repo and using into a local environment
```
git clone https://gitlab.com/momentofpeople/fnol-api.git
```
But, this is not your place to made push request, so, you must make a Fork of the project, you can follow an step by step instruction [here](https://blog.scottlowe.org/2015/01/27/using-fork-branch-git-workflow/)

Follow the steps:
```
* Making a local clone
$ git clone https://gitlab.com/mdklv-boilerplate-stack/api-restful-node.git
* Create a [Branch](https://12factor.net/codebase)
$ git checkout -b <<id-gitlab-ticket>>-my-feature
```

## About the infrastructure
#### Sentry distributed tracing and monitoring [sentry.io](https://sentry.io)
The docker-compose up --build made and mount the follow infrastructure

* API to save the form data based on , [node:14-slim](https://hub.docker.com/_/node) containerized
* Mongo database based on  [official container](https://hub.docker.com/_/mongo)



## Setup
### 1. Config environment
#### 1.1. The .env file
Setup environment variables for dev/qa, you can see an example on .env.example or simply override your .env file
```
$ cp .env.example .env
```
Setup your environment value for developement overriding when compile your code, used by the parameters

### 2. Install modules node_modules
Over the folder ./ run npm i
```
$ npm i
```

### 3. Mount the stack using docker-compose
Over the folder ./
```
$ docker-compose -f docker-compose.yml -f docker-compose.dev.yml --env-file .env up --build -d
```

To remove the container, over the folder ./
```
$ docker-compose down
```

### 4. Test
In your browser test the address, where port is the environment variable seeted
```
http://localhost:3001/api-docs/
```

In your console open the container logos
```
docker logs -f mdk-backend-api
```

If you use the mongo configured in mongo, you can access via
```
http://localhost:8081
```

Run a test
```
npm run test
```


##  Environment values reference

- **MONGO_DATABASE**:  db to save form data, related with the params above, eg: **modak**
- **MONGO_USERNAME**: user to save data on db, eg: **userdesa**
- **MONGO_PASSWORD**: password of MONGO_USERNAME, eg: **password**
- **MONGO_HOST**: mongodb://mongo:27017/modak (where mongo is the name of the container setted in docker-compose.dev.yml)
- **MONGO_PORT**: 27017
- **SENTRY_DSN**: sentry sdn url (optional)

- **HOST_CDN_STATIC**: logos cdn (optional)
- **NODE_ENV**: local

- **PORT**: application port vg **3001**
- **HOST**: application ip vg **0.0.0.0**

- **FIREBASE_CONFIG_JSON**: JSON firebase credential (optional)
- **FIREBASE_DATABASE_URL**: database user firebase (optional)




## Dev Tools
### Swagger UI
You can use swagger ui to test
```
http://localhost:3001/api-docs/
```

### Mongo admin
You can use mongo-express ui linked to mongo local
```
http://localhost:8081/
```
