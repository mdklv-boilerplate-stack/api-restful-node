var request = require('supertest');
const expect = require('chai').expect;
const should = require('chai').should;
const chai = require('chai');
const express = require('express');
const fs = require('fs');
const assert = require('assert').strict;
const dotenv = require('dotenv');
const jwt = require('jsonwebtoken');
const prettyjson = require('prettyjson');

dotenv.config();

const port = process.env.PORT;
const host = process.env.HOST || "0.0.0.0";
const default_timeout = 20000;

const chaiResponseValidator = require('chai-openapi-response-validator').default;
const url= `${host}:${port}`;

const getRandomInt = (min, max) => { return Math.floor(Math.random() * (max - min)) + min };
const vocal = () => { let v=[97,101,105,111,117]; let idx =  getRandomInt(0,4); return String.fromCharCode(v[idx]) ;}
const randomSpace = () => { let p = [1,1,1,0,0,0,0,0,0,0];let idx = getRandomInt(0,9); return p[idx]==1?" ":"";}
const randomChars = (max, includeSpaces = false) => {
    let chars = "";
    for(let i=0;i<max;i++) {
        chars+=  i%2?String.fromCharCode(getRandomInt( 97, 122 )):vocal() + ( includeSpaces?randomSpace():""); }
    return chars;
}

var randomUserName1 = "username."+ randomChars(4);
var user1Id = 1;
var randomUserName2 = "username2."+ randomChars(4);
var user2Id = 1;

var badUser1Id = '000000000000000000000000';
var randomLessonName1 = "lessonname."+ randomChars(4);
var lessonId = 1;
var randomLessonName2 = "lessonname2."+ randomChars(4);
var lessonId2 = 2;

let file = fs.readFileSync( './openapi.json','utf-8');
chai.use(chaiResponseValidator(JSON.parse(file)));
const app = express();
request = request.bind(request, url);  // add your url here

describe('Health Check',()=>{
    it('should response ok', () => {
        return request(app).get('/').then( (response) =>{
            expect(response.status).eq(200);
            expect(response.body).satisfySchemaInApiSpec('Healthcheck');
       })
    }).timeout( default_timeout );
});

describe('User', () => {
    it('create and should response bad request (400)', () => {
        return request(app).post('/user')
          .send( { name:'' })
          .then( (response) =>{
              expect(response.status).eq(400);
              expect(response.body).satisfySchemaInApiSpec('APIResponse');
          });
    }).timeout( default_timeout );

    it('create and should response succcefull (201)', () => {
        return request(app).post('/user')
          .send( { name:randomUserName1 })
          .then( (response) =>{
              expect(response.status).eq(201);
              expect(response.body).satisfySchemaInApiSpec('APIResponse');
              user1Id = response.body.payload;
          });
    }).timeout( default_timeout );

    it('create and should response succcefull (2) (201)', () => {
        return request(app).post('/user')
          .send( { name:randomUserName2 })
          .then( (response) =>{
              expect(response.status).eq(201);
              expect(response.body).satisfySchemaInApiSpec('APIResponse');
              user2Id = response.body.payload;
          });
    }).timeout( default_timeout );

    it('get and should response bad request (400)', () => {
        return request(app).get('/user/1')
          .then( (response) =>{
              expect(response.status).eq(400);
              expect(response.body).satisfySchemaInApiSpec('APIResponse');
          });
    }).timeout( default_timeout );

    it('get and should response succesfull (200)', () => {
        return request(app).get('/user/'+user1Id)
          .then( (response) =>{
              expect(response.status).eq(200);
              expect(response.body).satisfySchemaInApiSpec('User');
          });
    }).timeout( default_timeout );


    it('get and should response not found (404)', () => {
        return request(app).get('/user/'+badUser1Id)
          .then( (response) =>{
              expect(response.status).eq(404);
              expect(response.body).satisfySchemaInApiSpec('APIResponse');
          });
    }).timeout( default_timeout );

    it('list and should response items (200)', () => {
        return request(app).get('/user')
          .then( (response) =>{
              expect(response.status).eq(200);
              expect(response.body).to.be.an('array').that.is.not.be.empty;
              let item = response.body[0];
              expect(item).satisfySchemaInApiSpec('User');
          });
    }).timeout( default_timeout );
});

describe('Lesson', () => {
    it('create and should response bad request (400)', () => {
        return request(app).post('/lesson')
          .send( { name:'' })
          .then( (response) =>{
              expect(response.status).eq(400);
              expect(response.body).satisfySchemaInApiSpec('APIResponse');
          });
    }).timeout( default_timeout );

    it('create and should response succcefull (201)', () => {
        return request(app).post('/lesson')
          .send( { name:randomLessonName1 })
          .then( (response) =>{
              expect(response.status).eq(201);
              expect(response.body).satisfySchemaInApiSpec('APIResponse');
              lessonId = response.body.payload;
          });
    }).timeout( default_timeout );

    it('create and should response succcefull (2) (201)', () => {
        return request(app).post('/lesson')
          .send( { name:randomLessonName2 })
          .then( (response) =>{
              expect(response.status).eq(201);
              expect(response.body).satisfySchemaInApiSpec('APIResponse');
              lessonId2 = response.body.payload;
          });
    }).timeout( default_timeout );

    it('get and should response bad request (400)', () => {
        return request(app).get('/lesson/1')
          .then( (response) =>{
              expect(response.status).eq(400);
              expect(response.body).satisfySchemaInApiSpec('APIResponse');
          });
    }).timeout( default_timeout );

    it('get and should response succesfull (200)', () => {
        return request(app).get('/lesson/'+lessonId)
          .then( (response) =>{
              expect(response.status).eq(200);
              expect(response.body).satisfySchemaInApiSpec('Lesson');
          });
    }).timeout( default_timeout );

    it('get and should response not found (404)', () => {
        return request(app).get('/lesson/'+badUser1Id)
          .then( (response) =>{
              expect(response.status).eq(404);
              expect(response.body).satisfySchemaInApiSpec('APIResponse');
          });
    }).timeout( default_timeout );

    it('list and should response items (200)', () => {
        return request(app).get('/lesson')
          .then( (response) =>{
              expect(response.status).eq(200);
              expect(response.body).to.be.an('array').that.is.not.be.empty;
              let item = response.body[0];
              expect(item).satisfySchemaInApiSpec('Lesson');
          });
    }).timeout( default_timeout );
});


describe('UserLesson', () => {
  it('post and should response not found (400)', () => {
      return request(app).post('/user/1/lesson')
        .send( { id: '1' })
        .then( (response) =>{
            expect(response.status).eq(400);
            expect(response.body).satisfySchemaInApiSpec('APIResponse');
        });
  }).timeout( default_timeout );

  it('post and should response unprocessable entity (422)', () => {
      return request(app).post('/user/'+badUser1Id+'/lesson')
        .send( { id: badUser1Id })
        .then( (response) =>{
            expect(response.status).eq(422);
            expect(response.body).satisfySchemaInApiSpec('APIResponse');
        });
  }).timeout( default_timeout );

  it('post and should response succesfull (201)', () => {
      return request(app).post('/user/'+user1Id+'/lesson')
        .send( { id: lessonId })
        .then( (response) =>{
            expect(response.status).eq(201);
            expect(response.body).satisfySchemaInApiSpec('APIResponse');
        });
  }).timeout( default_timeout );

  it('post and should response succesfull (2) (201)', () => {
      return request(app).post('/user/'+user2Id+'/lesson')
        .send( { id: lessonId2 })
        .then( (response) =>{
            expect(response.status).eq(201);
            expect(response.body).satisfySchemaInApiSpec('APIResponse');
        });
  }).timeout( default_timeout );

  it('get user lesson should bad request (400)', () => {
      return request(app).get('/user/1/lesson')
        .then( (response) =>{
            expect(response.status).eq(400);
            expect(response.body).satisfySchemaInApiSpec('APIResponse');
        });
  }).timeout( default_timeout );

  it('get user lesson should response user not found (404)', () => {
      return request(app).get('/user/'+badUser1Id+'/lesson')
        .then( (response) =>{
            expect(response.status).eq(404);
            expect(response.body).satisfySchemaInApiSpec('APIResponse');
        });
  }).timeout( default_timeout );

  it('get user lesson should response a list (200)', () => {
      return request(app).get('/user/'+user1Id+'/lesson')
        .then( (response) =>{
          expect(response.status).eq(200);
          expect(response.body).to.be.an('array').that.is.not.be.empty;
          let item = response.body[0];
          expect(item).satisfySchemaInApiSpec('Lesson');
        });
  }).timeout( default_timeout );


});

describe('UserFriendShip', () => {
  it('post and should response not found (400)', () => {
      return request(app).post('/user/1/friend')
        .send( { id: '1' })
        .then( (response) =>{
            expect(response.status).eq(400);
            expect(response.body).satisfySchemaInApiSpec('APIResponse');
        });
  }).timeout( default_timeout );

  it('post and should response unprocessable entity (422)', () => {
      return request(app).post('/user/'+badUser1Id+'/friend')
        .send( { id: badUser1Id })
        .then( (response) =>{
            expect(response.status).eq(422);
            expect(response.body).satisfySchemaInApiSpec('APIResponse');
        });
  }).timeout( default_timeout );

  it('post and should response succesfull (201)', () => {
      return request(app).post('/user/'+user1Id+'/friend')
        .send( { to: user2Id })
        .then( (response) =>{
            expect(response.status).eq(201);
            expect(response.body).satisfySchemaInApiSpec('APIResponse');
        });
  }).timeout( default_timeout );

  it('post and should response succesfull (2) (201)', () => {
      return request(app).post('/user/'+user2Id+'/friend')
        .send( { to: user2Id })
        .then( (response) =>{
            expect(response.status).eq(201);
            expect(response.body).satisfySchemaInApiSpec('APIResponse');
        });
  }).timeout( default_timeout );

  it('get user friends should bad request (400)', () => {
      return request(app).get('/user/1/friend')
        .then( (response) =>{
            expect(response.status).eq(400);
            expect(response.body).satisfySchemaInApiSpec('APIResponse');
        });
  }).timeout( default_timeout );

  it('get user lesson should response user not found (404)', () => {
      return request(app).get('/user/'+badUser1Id+'/friend')
        .then( (response) =>{
            expect(response.status).eq(404);
            expect(response.body).satisfySchemaInApiSpec('APIResponse');
        });
  }).timeout( default_timeout );

  it('get user lesson should response a list (200)', () => {
      return request(app).get('/user/'+user1Id+'/friend')
        .then( (response) =>{
          expect(response.status).eq(200);
          expect(response.body).to.be.an('array').that.is.not.be.empty;
          let item = response.body[0];
          expect(item).satisfySchemaInApiSpec('User');
        });
  }).timeout( default_timeout );


});
