ARG APP_DIR="app"
# Node
FROM node:14-slim as build-image

# Global
ARG APP_DIR

# Metadata
LABEL maintainer="Pablo"
LABEL version="1.0"

# Crea directorio
RUN mkdir -p ${APP_DIR}

# Se estable el path
WORKDIR ${APP_DIR}

# Instala los paquetes existentes
COPY package.json .
RUN npm install 

# Instalación de Nodemon en forma Global
#RUN npm install nodemon -g --quiet

# Copia la Aplicación
COPY . .

# Expone la aplicación en el puerto 8000
EXPOSE 3000

# Inicia la aplicación al iniciar al contenedor
CMD ["node", "-r", "esm","src/app.js"]
